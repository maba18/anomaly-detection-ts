import numpy as np
import pandas as pd


def calc_slope(input_list):
    if sum(~np.isnan(x) for x in input_list) < 2:
        return np.NaN
    temp_list = input_list[:]
    max_value = np.nanmax(temp_list)
    max_index = np.where(input_list == max_value)[0][0]
    temp_list = np.delete(temp_list, max_index)
    second_max = np.nanmax(temp_list)
    second_max_index = np.where(input_list == second_max)[0][0]
    return (max_value - second_max)/(1.0*max_index-second_max_index)

# the below functions are coming from:
# https://github.com/blue-yonder/tsfresh/blob/master/tsfresh/feature_extraction/feature_calculators.py
# we can use any other similar function
def abs_energy(x):
    """
    Returns the absolute energy of the time series which is the sum over the squared values
    .. math::
        E = \\sum_{i=1,\ldots, n} x_i^2
    :param x: the time series to calculate the feature of
    :type x: numpy.ndarray
    :return: the value of this feature
    :return type: float
    """
    if not isinstance(x, (np.ndarray, pd.Series)):
        x = np.asarray(x)
    return np.dot(x, x)

def variance_larger_than_standard_deviation(x):
    """
    Boolean variable denoting if the variance of x is greater than its standard deviation. Is equal to variance of x
    being larger than 1
    :param x: the time series to calculate the feature of
    :type x: numpy.ndarray
    :return: the value of this feature
    :return type: bool
    """
    y = np.var(x)
    return y > np.sqrt(y)


def large_standard_deviation(x, r):
    """
    Boolean variable denoting if the standard dev of x is higher
    than 'r' times the range = difference between max and min of x.
    Hence it checks if
    .. math::
        std(x) > r * (max(X)-min(X))
    According to a rule of the thumb, the standard deviation should be a forth of the range of the values.
    :param x: the time series to calculate the feature of
    :type x: numpy.ndarray
    :param r: the percentage of the range to compare with
    :type r: float
    :return: the value of this feature
    :return type: bool
    """
    if not isinstance(x, (np.ndarray, pd.Series)):
        x = np.asarray(x)
    return np.std(x) > (r * (np.max(x) - np.min(x)))

def sum_of_reoccurring_values(x):
    """
    Returns the sum of all values, that are present in the time series
    more than once.
    :param x: the time series to calculate the feature of
    :type x: numpy.ndarray
    :return: the value of this feature
    :return type: float
    """
    unique, counts = np.unique(x, return_counts=True)
    counts[counts < 2] = 0
    counts[counts > 1] = 1
    return np.sum(counts * unique)


def roll(a, shift):
    """
    Roll 1D array elements. Improves the performance of numpy.roll() by reducing the overhead introduced from the
    flexibility of the numpy.roll() method such as the support for rolling over multiple dimensions.

    Elements that roll beyond the last position are re-introduced at the beginning. Similarly, elements that roll
    back beyond the first position are re-introduced at the end (with negative shift).

    Examples
    --------
    # >>> x = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    # >>> _roll(x, shift=2)
    # >>> array([8, 9, 0, 1, 2, 3, 4, 5, 6, 7])
    #
    # >>> x = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    # >>> _roll(x, shift=-2)
    # >>> array([2, 3, 4, 5, 6, 7, 8, 9, 0, 1])
    #
    # >>> x = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    # >>> _roll(x, shift=12)
    # >>> array([8, 9, 0, 1, 2, 3, 4, 5, 6, 7])

    Benchmark
    ---------


    :param a: the input array
    :type a: array_like
    :param shift: the number of places by which elements are shifted
    :type shift: int
    :return: shifted array with the same shape as a
    :return type: ndarray
    """
    if not isinstance(a, np.ndarray):
        a = np.asarray(a)
    idx = shift % len(a)
    return np.concatenate([a[-idx:], a[:-idx]])

def number_peaks(x, n):
    """
    Calculates the number of peaks of at least support n in the time series x. A peak of support n is defined as a
    subsequence of x where a value occurs, which is bigger than its n neighbours to the left and to the right.
    Hence in the sequence
    # >>> x = [3, 0, 0, 4, 0, 0, 13]
    4 is a peak of support 1 and 2 because in the subsequences
    # >>> [0, 4, 0]
    # >>> [0, 0, 4, 0, 0]
    4 is still the highest value. Here, 4 is not a peak of support 3 because 13 is the 3th neighbour to the right of 4
    and its bigger than 4.
    :param x: the time series to calculate the feature of
    :type x: numpy.ndarray
    :param n: the support of the peak
    :type n: int
    :return: the value of this feature
    :return type: float
    """
    x_reduced = x[n:-n]

    res = None
    for i in range(1, n + 1):
        result_first = (x_reduced > roll(x, i)[n:-n])

        if res is None:
            res = result_first
        else:
            res &= result_first

        res &= (x_reduced > roll(x, -i)[n:-n])
    return np.sum(res)


if __name__ == '__main__':

    window_size = 10
    df = pd.read_csv("input.csv", header=0)
    # df = pd.read_csv('input.csv', header=0, parse_dates=[0], index_col=0, squeeze=True) # read and convert to series
    df["Date"] = pd.to_datetime(df["Date"])
    df['mean'] = df['Data'].rolling(window=window_size, center=False).mean()
    df['median'] = df['Data'].rolling(window=window_size, center=False).median()
    df['min'] = df['Data'].rolling(window=window_size, center=False).min()
    df['max'] = df['Data'].rolling(window=window_size, center=False).max()
    df['var'] = df['Data'].rolling(window=window_size, center=False).var()
    df['std'] = df['Data'].rolling(window=window_size, center=False).std()
    df['skew'] = df['Data'].rolling(window=window_size, center=False).skew()
    df['kurt'] = df['Data'].rolling(window=window_size, center=False).kurt()
    df['kurt'] = df['Data'].rolling(window=window_size, center=False).skew()
    df['slope'] = df['Data'].rolling(window=window_size, center=False).apply(lambda x: calc_slope(x))
    df['abs_energy'] = df['Data'].rolling(window=window_size, center=False).apply(lambda x: abs_energy(x))
    df['variance_larger_than_standard_deviation'] = df['Data'].rolling(window=window_size, center=False).apply(lambda x: variance_larger_than_standard_deviation(x))
    df['large_standard_deviation'] = df['Data'].rolling(window=window_size, center=False).apply(lambda x: large_standard_deviation(x,2))
    df['sum_of_reoccurring_values'] = df['Data'].rolling(window=window_size, center=False).apply(lambda x: sum_of_reoccurring_values(x))

    df['number_peaks'] = df['Data'].rolling(window=window_size, center=False).apply(lambda x: number_peaks(x,3))




    # we can use this to fill previous NaNs if we want
    # df['rolling_mean_backfilled'] = df['mean'].fillna(method='backfill')
    # print(df.head(10))

    df.to_csv("output.csv")


from cesium import featurize
# features_to_use = ["amplitude",
#                    "percent_beyond_1_std",
#                    "maximum",
#                    "max_slope",
#                    "median",
#                    "median_absolute_deviation",
#                    "percent_close_to_median",
#                    "minimum",
#                    "skew",
#                    "std",
#                    "weighted_average"]

# References, Tutorials
# https://towardsdatascience.com/basic-time-series-manipulation-with-pandas-4432afee64ea